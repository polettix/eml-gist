#!/usr/bin/env perl
# vim: ts=3 sts=3 sw=3 et ai :

package EmailGist;
use 5.024;
use warnings;
use experimental qw< postderef signatures >;
no warnings qw< experimental::postderef experimental::signatures >;
use autodie;
use Pod::Usage qw< pod2usage >;
use Getopt::Long qw< GetOptionsFromArray :config gnu_getopt >;
use English qw< -no_match_vars >;
my $VERSION = '0.1';

use MIME::Parser;
use Path::Tiny;
use YAML::Dump 'Dump';
use Image::Magick;
use Try::Catch;
use Ouch ':trytiny_var';
use Log::Log4perl::Tiny qw< :dead_if_first :easy >;

use Exporter 'import';
our @EXPORT_OK = qw< email_gist >;

sub email_gist ($config) {
   my $self = bless {
      density => 192,
      size    => '840x1188',
      max_pages => 4,
      font => 'FreeMono',
      indent_unspaced => 50,
      indent_spaced   => 80,
      indent_space    => 10,
      indent_tab      => 30,
      $config->%*,
   }, __PACKAGE__;

   my $pdf = try {
      length(my $input = $self->{input} // '')
         or ouch 400, 'invalid input filename';
      my $basename = path($input)->basename;

      my $tempdir = Path::Tiny->tempdir; # everything is put here

      # parse the incoming message and get a MIME::Entity back, with
      # actual data in files inside $tempdir
      my $parser = MIME::Parser->new;
      $parser->output_dir($tempdir);
      $self->{entity} = my $entity = $parser->parse_open($input);

      # start aggregating stuff into an image
      my $magick = $self->{magick} //= $self->new_magick;

      $self->add_parts;
      $self->add_message_structure;
      $self->add_headers;

      # adjust size of final product
      $magick->Resize($self->{size});

      return $self->{target} ? $self->save_pdf : $self->as_pdf;
   }
   catch {
      ouch 400, bleep;
   };

   return $pdf;
}

sub add_message_structure ($self) {
   open my $fh, '>:encoding(utf-8)', \my $buffer;
   $self->{entity}->dump_skeleton($fh);
   close $fh;
   $self->add_text('Message structure', $buffer);
}

sub add_headers ($self) {
   $self->add_text(q{Intestazioni nell'email}, $self->headers_as_string);
}

sub as_pdf ($self) {
   my $filename = Path::Tiny->tempfile('gist-XXXXXXX', SUFFIX => '.pdf');
   $self->save_pdf($filename);
   return $filename->slurp_raw;
}

sub save_pdf ($self, $filename = undef) {
   $self->{magick}->Write($filename // $self->{target});
   return $filename;
}

sub new_magick ($self) {
   my $magick = Image::Magick->new;
   $magick->Set(density => $self->{density});
   $magick->Set(size => $self->{size});
   return $magick;
}

sub headers_as_string ($self) {
   my $head = $self->{entity}->head;
   return join "\n", sort { $a cmp $b } map {
      my $name = $_;
      map { $name . ': ' . $_ } $head->get_all($_);
   } $head->tags;
}

sub guess_type ($self, $entity) {
   my $head = $entity->head;
   my $content_type = $head->mime_type;
   return 'txt' if $content_type =~ m{\A text/plain}imxs;
   return 'html' if $content_type =~ m{\A text/html}imxs;
   return 'img' if $content_type =~ m{\A application/pdf}imxs;
   return 'img' if $content_type =~ m{\A image/}imxs;
   return 'img' if lc($head->recommended_filename) =~
      m{(?mxs: \.(pdf|jpe?g|png)\z)};
   return $content_type;
}

sub add_text ($self, $title, $text) {
   my $magick = $self->{magick};
   my $x_unspaced = $self->{indent_unspaced};
   my $x_spaced = $self->{indent_spaced};
   my $x_delta_space = $self->{indent_space};
   my $x_delta_tab = $self->{indent_tab};
   my $font = $self->{font};

   my @lines = split m{\n}mxs, $text;
   @lines = ('') unless @lines;
   my $suffix = '';
   while (@lines) {
      $magick->Read('xc:white');
      my $page = $magick->[-1];

      $page->Annotate(
         text => "$title$suffix",
         font => $font,
         pointsize => 32,
         geometry => '+0+40',
         gravity => 'North',
      );

      my $y = 120;
      for my $line (splice @lines, 0, 50) {
         my ($spaces, $rest) = $line =~ m{\A (\s*) (.*)}mxs;
         my $x = $x_unspaced;
         for my $space (split m{}mxs, ($spaces // '')) {
            $x += $space eq "\t" ? $x_delta_tab : $x_delta_space;
         }
         DEBUG "+$x+$y $line";
         $page->Annotate(
            text => $rest,
            font => $font,
            pointsize => 14,
            geometry => "+$x+$y",
         );
         $y += 20;
      }

      $suffix = ' (continued)';
   }

   return $magick;
}

sub _gather_parts ($self, $entity = undef) {
   $entity //= $self->{entity};
   my $max_pages = $self->{max_pages} - 1; # starting from 0
   my $magick = $self->{magick};
   my @parts;
   if (defined(my $bh = $entity->bodyhandle)) {
      my $path = $bh->path;
      my $type = $self->guess_type($entity);
      INFO "type $type for $path";
      if ($type eq 'img') {
         my $imgs = $path . "[0-$max_pages]";
         return [ img => sub {$magick->Read($imgs) } ];
      }
      elsif ($type eq 'txt' || $type eq 'html') {
         require Text::Wrap;
         my $text = Text::Wrap::fill('', '', path($path)->slurp_utf8);
         return [
            $type => sub { $self->add_text(path($path)->basename, $text) }
         ];
      }
      else {
         WARN "unhandled type '$type'";
         my $filename = path($path)->basename;
         return [ $type => sub { $self->add_text($filename, "Attachment of type $type")} ];
      }
   }
   else {
      @parts = map { __SUB__->($self, $_) } $entity->parts;
   }
   return @parts;
}

sub add_parts ($self) {
   my @parts = $self->_gather_parts;
   INFO join ', ', map { $_->[0] } @parts;
   my @rest;
   for my $type (qw< txt img >) {
      for my $part (@parts) {
         if ($part->[0] eq $type) {
            INFO "part<$part->[0]>";
            $part->[1]->();
         }
         else {
            push @rest, $part;
         }
      }
      (@parts, @rest) = @rest;
   }
   if (@parts) {
      $self->add_text('Altro materiale', q{*** Da qui si fa un po' tecnico ***});
      $_->[1]->() for @parts;
   }
   return $self;
}

1;
