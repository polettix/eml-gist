package EmailGistWeb;
use v5.24;
use Mojo::Base 'Mojolicious', -signatures;
use Ouch ':trytiny_var';
use Try::Catch;
use Mojo::Util qw< b64_decode >;
use EmailGistWeb::Controller;

use constant MONIKER => 'eml-gist';
use constant DEFAULT_SECRETS => 'MTY1ODAwNDkzNS0wLjMyNDUyOTIxNTczNDQ3OQ==';

has 'conf';

sub startup ($self) {
   $self->moniker(MONIKER);
   $self->_startup_config
      ->_startup_routes
      ->_startup_secrets;
   $self->controller_class('EmailGistWeb::Controller');
   $self->log->info('startup complete');
   return $self;
}

sub _startup_config ($self) {
   my $config = eval { $self->plugin('NotYAMLConfig') } || {};
   my $prefix = (uc(MONIKER) =~ s{-}{_}rgmxs) . '_';
   for my $key (qw<
         density size max_pages font indent_unspaced indent_spaced
      >) {
      my $env_key = $prefix . uc($key);
      $config->{$key} = $ENV{$env_key} if defined $ENV{$env_key};
   }
   $self->conf($config);
   return $self;
}

sub __split_and_decode ($s) { map { b64_decode($_) } split m{\s+}mxs, $s }

sub _startup_secrets ($self) {
   my $config = $self->conf;
   my @secrets =
       defined $ENV{SECRETS}      ? __split_and_decode($ENV{SECRETS})
     : defined $config->{secrets} ? $config->{secrets}->@*
     :                              __split_and_decode(DEFAULT_SECRETS);
   $self->secrets(\@secrets);
   return $self;
} ## end sub _startup_secrets

sub _startup_routes ($self) {
   my $r = $self->routes;
   $r->any('/')->to('basic#root');
   $r->post('/eml-gist')->to('basic#gist');
   return $self;
}

1;
