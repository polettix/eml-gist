package EmailGistWeb::Controller::Basic;
use Mojo::Base 'EmailGistWeb::Controller', -signatures;
use Mojo::File 'tempfile';
use Log::Log4perl::Tiny qw< :easy >;

use EmailGist 'email_gist';

sub root ($self) { return $self->render(template => 'form') }

sub gist ($self) {
   my $email = $self->param('email');
   my $email_file = tempfile();
   $email->move_to($email_file);
   my $data = email_gist(
      {
         density => $self->param('density') // 192,
         max_pages => $self->param('max_pages') // 4,
         size => $self->param('size') // '840x1188',
         input => $email_file,
         main_name => Mojo::File->new($email->filename)->basename,
      }
   );
   INFO "size of data: " . length($data//='');

   my $file = Mojo::File->new($email->filename)->basename . '.pdf';
   $file =~ s{[^-\w.]+}{}gmxs;
   INFO "filename '$file'";

   my $headers = $self->res->headers;
   $headers->content_disposition("attachment; filename=$file;");
   $headers->content_type('application/pdf');
   return $self->render(data => $data);
}

1;
