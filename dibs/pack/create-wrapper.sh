#!/bin/sh
target="$1"
image="$2"

# ensure there's the directory for the target
mkdir -p "$(dirname "$target")"

# print the wrapper script to the target. We divide the print in two steps
# to limit the need for escaping "$" and other shell-special characters
# as much as possible
{
   # here we need to print out the contents of the $image variable, so
   # the heredoc allows expansion of variables. This means that we have
   # to escape all other special characters.
   cat <<END
#!/bin/sh
: \${IMAGE:="$image"}
END

   # here we don't need to expand variables any more so we set the
   # heredoc delimiter with single quotes
   cat <<'END'
if [ -z "$(docker image ls -q "$IMAGE")" ] ; then
   cat >&2 <<__END__

The default image name is set to $IMAGE but this does not exist in this
system. Please re-tag the correct image, or set environment variable
IMAGE to point to the right image (including the tag).

__END__
   exit 1
fi

# Add option "-it" to the docker command line for interactive sub-commands
: ${DOCKER_OPTS:=""}
if [ $# -gt 0 ] ; then
   interactive_subcommands='shell'
   for cmd in $interactive_subcommands ; do
      [ "$1" = "$cmd" ] || continue
      DOCKER_OPTS="$DOCKER_OPTS -it"
      break
   done
fi

docker run --rm $DOCKER_OPTS "$IMAGE" "$@"
END
} > "$target"

chmod +x "$target"

exit 0
