#!/bin/sh
app_dir="$1"
version="$2"
moniker="${3:-""}"

src_dir="$(readlink -f "$(cat DIBS_DIR_SRC)")"
pandoc="$src_dir/support/pandoc"
docs_dir="$src_dir/docs"
dst_dir="$(readlink -f "$app_dir")/docs/text"
[ -n "$moniker" ] && moniker="$moniker-"

id_length=0
for filename in "$docs_dir"/*.md ; do
   base="$(basename "$filename")"
   base="${base#*.}"
   id="${base%%.*}"
   len="$(printf %s "$id" | wc -c)"
   [ "$len" -le "$id_length" ] || id_length="$len"
done

mkdir -p "$dst_dir"
{
   printf "%${id_length}s - %s\\n" ID Title

   for filename in "$docs_dir"/*.md ; do
      base="$(basename "$filename")"
      base="${base#*.}"
      id="${base%%.*}"
      title="${base#*.}"
      title="${title%.md}"

      printf "%${id_length}s - %s\\n" "$id" "$title"

      {
         printf '%s\n%s\n%s\n\n' \
            "% $moniker$id(1) prototype web services | $version" \
            "% polettix" \
            "% $(date +'%Y-%m-%d')"
         cat "$filename"
      }  | "$pandoc" -s -f markdown -t man \
         | groff -T utf8 -man - \
         > "$dst_dir/$id.txt"
   done
} > "$dst_dir/list"

exit 0
