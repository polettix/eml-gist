# eml-gist - Helm chart

This is an [Helm][] chart to deploy the `eml-gist` [Docker][] image in
[Kubernetes][]. It is expected to work fine with Helm version 3.

The contents of this Helm chart are Copyright 2022 by Flavio Poletti.

[Helm]: https://helm.sh/
[Docker]:  https://www.docker.com/
[Kubernetes]: https://kubernetes.io/
