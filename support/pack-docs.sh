#!/bin/sh
dst_dir="$1"
version="$2"
md="$(dirname "$0")"
name="$(cat "$md/NAME")"
tar cC "$dst_dir/../docs" text | tar xC "$dst_dir"
cd "$dst_dir"
mv text "$name"-docs-text
tar czf docs-text.tar.gz "$name"-docs-text
rm -rf "$name"-docs-text
exit 0
