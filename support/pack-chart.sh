#!/bin/sh
dst_dir="$1"
version="$2"
md="$(dirname "$0")"
name="$(cat "$md/NAME")"
tar cC "$md" chart | tar xC "$dst_dir"
cd "$dst_dir"
mv chart "$name"-chart
sed -i -e "s/__VERSION__/$version/g" "$name"-chart/values.yaml
tar czf chart.tar.gz "$name"-chart
rm -rf "$name"-chart
exit 0
