requires 'perl', '5.024000';
requires 'IO::Socket::SSL';
requires 'Log::Log4perl::Tiny';
requires 'MIME::Tools';
requires 'Mojolicious';
requires 'Ouch';
requires 'Path::Tiny';
requires 'Text::Wrap';
requires 'Try::Catch';
requires 'YAML::Dump';

on test => sub {
   requires 'Path::Tiny',      '0.084';
};

on develop => sub {
   requires 'Path::Tiny',          '0.084';
   requires 'Template::Perlish',   '1.52';
   requires 'Test::Pod::Coverage', '1.04';
   requires 'Test::Pod',           '1.51';
};
