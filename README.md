Get gist of an email as a PDF, both as a command-line program and as a Web
Application (thanks to [Mojolicious][]).


# INSTALLATION

The most straightforward way to instal `eml-gist` is through the [Docker
image][]:

```
BINDIR="$HOME/bin"   # set to your preference...
docker pull polettix/eml-gist
docker run --rm polettix/eml-gist wrapper > "$BINDIR"/eml-gist
chmod +x "$BINDIR"/eml-gist
```

Make sure that file `$BINDIR/eml-gist` contains the right image name before
using it.

This is basically it and provides a quick access to the web service:

```
# assuming it's in PATH...
DOCKER_OPTS='-p 3000:3000' eml-gist service
```

By default the service binds to port 3000. The command is the same as the
following:

```
docker run --rm -p 3000:3000 polettix/eml-gist service
```


# DOCKER IMAGE

The repository allows generating a new image if things change, using [dibs][].


# COPYRIGHT & LICENSE

This repository contains artefacts developed for the repository itself,
addressed in sub-section `Main` below, as well as a statically compiled version
of [Pandoc][], addressed in its own section.

## Main

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2022 by Flavio Poletti (flavio@polettix.it).
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.


## Pandoc

The repository contains a statically compiled copy of [Pandoc][]:

```
$ ./pandoc -v
pandoc 2.11.3.2
Compiled with pandoc-types 1.22, texmath 0.12.1, skylighting 0.10.2,
citeproc 0.3.0.3, ipynb 0.1.0.1
User data directory: ...
Copyright (C) 2006-2020 John MacFarlane. Web:  https://pandoc.org
This is free software; see the source for copying conditions. There is no
warranty, not even for merchantability or fitness for a particular purpose.
```

The copying conditions are available in [Pandoc's repository][pandocrep] as
file [COPYRIGHT][pandoccopy] and also as file `PANDOC_COPYRIGHT`.

[Mojolicious]: https://metacpan.org/pod/Mojolicious
[Pandoc]: https://pandoc.org/
[pandocrep]: https://github.com/jgm/pandoc
[pandoccopy]: https://github.com/jgm/pandoc/blob/2.11.3.2/COPYRIGHT
[Docker image]: https://hub.docker.com/r/polettix/eml-gist
[dibs]: http://blog.polettix.it/hi-from-dibs/
